//
//  Contestant.swift
//  Me First Me First
//
//  Created by Jonathan Wong on 8/5/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class Contestant {
    var BaseNode : SKNode
    var Sprite : SKSpriteNode
    var Touch : UITouch
    var sequence : [String] = ["Flash/flash00.png", "Explosion/explosion00.png", "Explosion/explosion02.png", "Black smoke/blackSmoke00.png", "Black smoke/blackSmoke01.png",
        "Black smoke/blackSmoke02.png", "Black smoke/blackSmoke03.png"]
    var seqCount : Int = 0
    
    var validPlanets = ["1", "2", "3", "4", "5", "6", "7",
        "10", "11", "12", "13", "14", "15", "16", "17", "18_0", "19", "20"]
    
    init(touch : UITouch) {
        BaseNode = SKNode()
        
        var rand = random() % validPlanets.count
        Sprite = SKSpriteNode(imageNamed: "planet\(validPlanets[rand])");
        Sprite.setScale(0.75)
        BaseNode.addChild(Sprite);
        Touch = touch;
    }
    func select() {
        
    }
    
    func explode() {
        let explosionBlock = SKAction.runBlock(runExplosionAnimation);
        let delay = SKAction.waitForDuration(0.05);
        
        let seq = SKAction.sequence([explosionBlock, delay])
        let repeat = SKAction.repeatAction(seq, count: sequence.count + 1)
        
        self.BaseNode.runAction(repeat)
    }
    
    func runExplosionAnimation() {
        Sprite.removeFromParent()
        if(seqCount < sequence.count)
        {
            Sprite = SKSpriteNode(imageNamed: sequence[seqCount]);
            Sprite.setScale(CGFloat(1.0 / Double(seqCount)))
            BaseNode.addChild(Sprite);
        }
        seqCount++
    }
}