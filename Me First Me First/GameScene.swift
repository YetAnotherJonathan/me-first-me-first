//
//  GameScene.swift
//  Me First Me First
//
//  Created by Jonathan Wong on 8/6/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    var contestants : [Contestant] = []
    var myLabel = SKLabelNode(fontNamed:"Chalkduster")
    var timer = NSTimer()
    var maxTime = 3
    var timeLeft : Int = 0
    var timerIsRunning : Bool = false
    var validContestants : Bool = false
    var winnerSelected = false
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        backgroundColor = UIColor(red: 0.01, green: 0.01, blue: 0.2, alpha: 1.0)
        myLabel.fontSize = 48;
        myLabel.text = "Touch to start";
        
        myLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame));
        timeLeft = maxTime
        
        initStars()
        
        self.addChild(myLabel)
    }
    
    func initStars() {
        for i in 0..<50 {
            let star = SKSpriteNode(imageNamed: "star1.png")
            let scale =
            //            CGFloat(2.0)
            ((CGFloat(rand()) % 100) / 200) + 0.1
            
            star.setScale(scale)
            star.position.x = CGFloat(rand()) % size.width
            star.position.y = CGFloat(rand()) % size.height
            let zRotation = CGFloat(rand()) % 1.5707
            star.zRotation = zRotation
            
            let star2 = SKSpriteNode(imageNamed: "star1.png")
            
            //            star2.setScale(scale)
            star2.zRotation = zRotation + 1.5707
            
            star.addChild(star2);
            self.addChild(star)
        }
    }
    
    func startCountDown() {
        timeLeft = maxTime
        
        myLabel.text = "Starting..."
        if(!timerIsRunning)
        {
            timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("countDown"), userInfo: nil, repeats: true)
            timerIsRunning = !timerIsRunning
        }
    }
    
    func countDown() {
        if winnerSelected {
            myLabel.text = ""
            return;
        }
        if validContestants {
            myLabel.text = "Time Left: \(timeLeft)"
            if timeLeft > 0
            {
                timeLeft--;
            }
            else
            {
                explode()
                myLabel.text = ""
            }
        }
        else {
            myLabel.text = "Touch to start"
        }
        
    }
    
    func explode() {
        if(contestants.count <= 1) {
            return;
        }
        var spareMe = random() % contestants.count
        
        var spared = contestants[spareMe];
        
        
        for i in 0..<contestants.count {
            if( i != spareMe)
            {
                contestants[i].explode()
            }
        }
        
        contestants.removeAll(keepCapacity: false)
        contestants.append(spared)
        
        
        winnerSelected = true
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        /* Called when a touch begins */
        
        for touch in (touches as! Set<UITouch>) {
            var contestant = Contestant(touch: touch)
            contestants.append(contestant)
            self.addChild(contestant.BaseNode)
            contestant.BaseNode.position = touch.locationInNode(self)
            
        }
        
        if(contestants.count > 1) {
            validContestants = true
        }
        winnerSelected = false
        startCountDown()
    }
    
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
        for touch in (touches as! Set<UITouch>) {
            for contestant in contestants {
                if touch == contestant.Touch {
                    contestant.BaseNode.position = touch.locationInNode(self)
                }
            }
        }
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        for touch in (touches as! Set<UITouch>) {
            var contestantIndex = 0
            for contestant in contestants {
                if touch == contestant.Touch {
                    contestant.BaseNode.removeFromParent()
                    contestants.removeAtIndex(contestantIndex)
                    break;
                }
                contestantIndex++
            }
        }
        if(contestants.count <= 1 ) {
            validContestants = false;
            timeLeft = maxTime;
        }
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
